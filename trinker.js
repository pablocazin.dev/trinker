const { sort, pop } = require("./people");
const people = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  convertirAge: function(x) {
    return  (new Date() - new Date(x.date_of_birth)) / 31557600000;
  },

  pythagore: function (a, b) {
    return Math.sqrt((b.long - a.long) ** 2 + (b.lat - a.lat) ** 2);
  },

  isKeyExists: function(obj,key){
    if( obj[key] == undefined ){
        return false;
    } else{
        return true;
    }
  },

  moyenne: function(a) {
    count = 0; for(let i of a) { count += i} return count/a.length;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function (p) {
    let x = [];
    for (let i of p) {
      if (i.gender === "Male") {
        x.push(i);
      }
    }
    return x;
  },

  allFemale: function (p) {
    let x = [];
    for (let i of p) {
      if (i.gender === "Female") {
        x.push(i);
      }
    }
    return x;
  },

  nbOfMale: function (p) {
    return this.allMale(p).length;
  },

  nbOfFemale: function (p) {
    return this.allFemale(p).length;
  },

  nbOfMaleInterest: function (p) {
    let nb = 0;
    for (let i of p) {
      if (i.looking_for === "M") {
        nb++;
      }
    }
    return nb;
  },

  nbOfFemaleInterest: function (p) {
    return p.length - this.nbOfMaleInterest(p);
  },

  nbAbove2: function (p) {
    let nb = 0;
    for (let i of p) {
      if (parseFloat(i.income.substring(1)) > 2000) {
        nb++;
      }
    }
    return nb;
  },

  nbDrama: function (p) {
    let count = 0;
    for (let i of p) {
      const pref = i.pref_movie;
      const arr = pref.split("|");
      if (arr.includes("Drama")) {
        count++;
      }
    }
    return count;
  },

  womanSf: function (p) {
    let count = 0;
    for (let i of this.allFemale(p)) {
      if (i.pref_movie.includes("Sci-Fi")) {
        count++;
      }
    }
    return count;
  },

  docAndMoney: function (p) {
    let count = 0;
    for (let i of p) {
      if (
        i.pref_movie.includes("Documentary") &&
        i.income.substring(1) > 1482
      ) {
        count++;
      }
    }
    return count;
  },
  aboveFourList: function (p) {
    let x = [];
    for (let i of p) {
      if (parseFloat(i.income.substring(1)) > 4000) {
        x.push(i.last_name, i.first_name, i.id, i.income);
      }
    }
    return x;
  },

  maxMoney: function (p) {
    let max = 0;
    let richMan;
    for (let i of this.allMale(p)) {
      if (parseFloat(i.income.substring(1)) > max) {
        max = i.income.substring(1);
        richMan = [i.last_name, i.id];
      }
    }
    return `${richMan} gagne ${max}$`;
  },

  mediumMoney: function (p) {
    let totalMoney = 0;
    for (let i of p) {
      totalMoney += parseFloat(i.income.substring(1));
    }
    result = totalMoney / p.length;
    return result;
  },

  median: function (p) {
    array = [];
    for (let i of p) {
      array.push(parseFloat(i.income.substring(1)));
    }
    array.sort((a, b) => a - b);
    let result = 0;

    if (array.length % 2 == 0) {
      /*pair*/
      result = (array[array.length / 2] + array[array.length / 2 + 1]) / 2;
    } else {
      result = array[array.length / 2];
    }
    return result;
  },

  latitude: function (p) {
    let count = 0;
    for (let i of p) {
      if (i.latitude > 0) {
        count++;
      }
    }
    return count;
  },

  mediumIncomesSouthPeople: function (p) {
    let array = [];
    for (let i of p) {
      if (i.latitude < 0) {
        array.push(parseFloat(i.income.substring(1)));
      }
    }
    let count = 0;
    for (let x of array) {
      count += x;
    }
    let result = count / array.length;
    return result;
  },

  salutBerenice: function (p, nom = "Cawt", prenom ="Bérénice") {
    let a = { long: 0, lat: 0 };
    for (let x of p) {
      if (x.last_name == nom && x.first_name == prenom) {
        a.long = x.longitude;
        a.lat = x.latitude;
      }
    }
    let gap = 999999999999.0;
    let nearest_User = [];
    for (let i of p) {
      let b = { long: i.longitude, lat: i.latitude };
      if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
        gap = this.pythagore(a, b);
        nearest_User = [i.last_name, i.id];
      }
    }
    return nearest_User;
  },

  salutBrach: function (p) {
    let a = { long: 0, lat: 0 };
    for (let x of p) {
      if (x.last_name == "Brach" && x.first_name == "Ruì") {
        a.long = x.longitude;
        a.lat = x.latitude;
      }
    }
    let gap = 999999999999.0;
    let nearest_User = [];
    for (let i of p) {
      let b = { long: i.longitude, lat: i.latitude };
      if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
        gap = this.pythagore(a, b);
        nearest_User = [i.last_name, i.id];
      }
    }
    return nearest_User;
  },

  salutJosee: function (p) {
    let a = { long: 0, lat: 0 };
    for (let x of p) {
      if (x.last_name == "Boshard" && x.first_name == "Josée") {
        a.long = x.longitude;
        a.lat = x.latitude;
      }
    }
    let users_gap = [];
    for (let i of p) {
      let b = { long: i.longitude, lat: i.latitude };
      if (this.pythagore(a, b) !== 0) {
        users_gap.push(this.pythagore(a, b));
      }
    }
    users_gap.sort((a, b) => a - b);
    users_gap.splice(10, p.length - 10);
    let topTen = [];
 
    for(let i of users_gap) {
        for (let x of p) {
            let b = { long: x.longitude, lat: x.latitude };
            if (this.pythagore(a, b) !== 0 && this.pythagore(a, b) == i) {
              topTen.push({ nom: x.last_name, id: x.id });
            }
          }
    }
    return topTen;
  },

  googlist: function(p) {
      result = [];
      for(let x of people) {
          if(x.email.includes("google")) {
              result.push(({ "nom": x.last_name, "id": x.id }))
          }
      }
      return result;
  },

    vieux: function (p) {
        let t=[]
        for (let i of p){
            t.push({ddn:new Date(i.date_of_birth),name:i.last_name})
            t.sort((a, b) => a.ddn- b.ddn);
        } 
    return t[0]
    },
    

    jeune: function (p) {
        let t=[]
        for (let i of p){
            t.push({ddn:new Date(i.date_of_birth),name:i.last_name})
            t.sort((a, b) => a.ddn- b.ddn);
        } 
    return t[t.length-1]
    },

  averageAgeGap: function(p) {
    allAverage = [];
    allAges = [];
    for(let x of p) {
        allAges.push(this.convertirAge(x));
    }
    let ageGaps = [];
    for(let x of allAges) {
        allAges.splice(allAges.indexOf(x), 1);
        for(let i of allAges) {
            ageGaps.push(Math.abs(x - i))
        }
    }
    let count = 0;
    for(let i of ageGaps) {
        count += i;
    }
    let averageGap = count/ageGaps.length;

      return averageGap + " ans.";
  },

    genrePopulaireList: function(p) {
        allCategory = {};
      
        for(let i of p) {
            let genres = i.pref_movie.split("|");
            for(let x of genres) {
                if(this.isKeyExists(allCategory, x)) {
                    allCategory[x]+= 1;
                } else {
                    allCategory[x] = 1;
                }
            }
        }
        let sortCat = Object.entries(allCategory).sort((a, b) => b[1] - a[1]);
        return sortCat;
    },

  genrePopulaire: function(p) {
    return this.genrePopulaireList(p)[0];
  },

  ordrePopulaire: function(p) {
      let ordre = [];
      let count = 1;
      for(let i of this.genrePopulaireList(p)){
          ordre.push(`${count}: ${i[0]}`)
          count++
      }
      return ordre;
  },

  OrdrePopulaireNombre: function(p) {
    let ordre = [];
    let count = 1;
    for(let i of this.genrePopulaireList(p)){
        ordre.push(`${count}: ${i[0]}, est aimé par ${i[1]} personnes.`)
        count++
    }
    return ordre;
  },

  ageMoyenHommeFilmNoir: function (p) {
    let user = [];
    for(let i of this.allMale(p)) {
        if(i.pref_movie.includes("Film-Noir")) {
            user.push(this.convertirAge(i))
        }
    } 
    return `${this.moyenne(user)} ans`
  },

  AMFFNPGMMH: function (p) {
   /* for(let i of this.allFemale(p)) {
        if(i.pref_movie.includes("Film-Noir") && 
        ){}
    }*/
    return new Date().toISOString()
  },

  match: function (p) {
    return "not implemented".red;
  }
};

